import copy

truthAssignment = []

def parseCNFInput(stringInput):
    # Checking that the first and last characters are opening and closing brackets respectively
    if (stringInput[0] == "(" and stringInput[len(stringInput) - 1] == ")"):
        cnf = stringInput[1:len(stringInput) - 1]
    else:
        # If they are not an exception is raised
        raise Exception("Error in Input")

    clauses = []

    # Splitting the input based on the ")(" between each clause
    ret = cnf.split(")(")

    # Going through each item in the split list, which is each clause
    for clause in ret:
        # Each clause is further split into individual literals
        literals = clause.split(",")

        # Removing any whitespace
        for l, literal in enumerate(literals):
            literal = literal.strip()
            # Reassigning the literal with no white space
            literals[l] = literal

            # Removing the negation to check for erroneous input
            if literal[0] == "!":
                literal = literal[1:]

            # Checking that each literal is formed by a number of letters not (')') which is an erroneous input
            if not literal.isalpha():
                raise Exception("Error in Input")

        # The list of individual literals is added to the clauses list
        # The result is a list having a list for each clause with an element for each literal in each clause list
        clauses.append(literals)

    return clauses


def addLiteralToDict(literal, clausePosition, literalPosition, dict):
    # -1 is returned when the opposite literal to the one entered was not already entered
    # else the clauses position where the opposite literal was found is returned

    # Generating the negation of the inputted literal
    if literal[0] == '!':
        oppositeLiteral = literal[1:]
    else:
        oppositeLiteral = "!" + literal

    # If the current literal was not already added to the dictionary
    if dict.get(literal) is None:
        # Checking if an opposite literal in a different clause was found
        if dict.get(oppositeLiteral) is not None:
            # Going through the clause positions where the opposite literal was found
            for i in range(0, len(dict[oppositeLiteral]), 2):
                # If not found in the same clause return the clause position it was found in
                if dict[oppositeLiteral][i] != clausePosition:
                    return i

        # Adding the new literal to the dictionary if an opposite literal was not found
        # Value is a list (clausePosition, literalPosition)
        dict[literal] = [clausePosition, literalPosition]


    else:
        # Checking if an opposite literal in a different clause was found
        if dict.get(oppositeLiteral) is not None:
            # Going through the clause positions where the opposite literal was found
            for i in range(0, len(dict[oppositeLiteral]), 2):
                # If not found in the same clause return the clause position it was found in
                if dict[oppositeLiteral][i] != clausePosition:
                    return i

        # Adding the new entries to the dictionary value
        dict[literal].append(clausePosition)
        dict[literal].append(literalPosition)

    return -1


def exhaustivelyApply1LiteralRule(unitClause, clauses):
    change = True
    while change:
        change = False
        # Obtaining the negation of the unitClause literal  (to remove it from the other clauses)
        if unitClause[0] == "!":
            literalToRemove = unitClause[1:]
        else:
            literalToRemove = "!" + unitClause

        # Going through all the clauses and their literals
        for clause in clauses:
            for literal in clause:
                # If the literal is the negation of the literal found in the unit clause, remove the literal from the clause
                if literal == literalToRemove:
                    clause.remove(literal)
                    change = True

                # If the literal found is the same as the one found in the unit clause, the whole clause is removed
                elif literal == unitClause:
                    clauses.remove(clause)
                    change = True

    return clauses


def pureLiteralRuleHelper(literal, clausePosition, literalDict):
    # Adding all the literals to a dictionary where the first element of the list indicates whether they are positive (1) or negative (-1)
    # -2 indicates that the opposite literal has been found, and hence the literal is not a pure one.

    tempLiteral = literal
    negation = False
    # Removing the negation if it is a negative literal
    if literal[0] == '!':
        tempLiteral = literal[1:]
        negation = True

    if literalDict.get(tempLiteral) == None:
        # If the literal was not inserted before
        if negation:
            # Value is a list [literalType, clausePosition]
            # If the literal was negative literalType = -1
            # If literal was positive literalType = 1
            literalDict[tempLiteral] = [-1, clausePosition]
        else:
            literalDict[tempLiteral] = [1, clausePosition]

    else:
        # If the literal has not already been flagged as not being a pure literal
        if literalDict[tempLiteral][0] != -2:

            # Checking if the previous literal which was found is the negation (opposite) of the one found, in that case
            # the literal is not a pure one
            if (negation and literalDict[tempLiteral][0] == 1) or (not negation and literalDict[tempLiteral][0] == -1):
                # Literal with opposite signs have been found
                literalDict[tempLiteral][0] = -2
            elif (negation and literalDict[tempLiteral][0] == -1) or (
                    not negation and literalDict[tempLiteral][0] == 1):
                # Adding the clause position where the (till now) pure literal is found
                literalDict[tempLiteral].append(clausePosition)


def exhaustivelyApplyPureLiteralRule(clauses):
    pureLiteralFound = True
    while (pureLiteralFound):
        pureLiteralFound = False
        literalDict = {}
        for c, clause in enumerate(clauses):
            for literal in clause:
                # Adding all the literals
                pureLiteralRuleHelper(literal, c, literalDict)


        output = set()
        # Going through all the literals appended to the dictionary
        for lit in literalDict:
            # -2 means that the literal is not pure (i.e. two opposite literals have been found)
            if literalDict[lit][0] != -2:

                if literalDict[lit][0] == -1:
                    # If the literal found was the negation
                    truthAssignment.append("!" + lit)
                else:
                    # If the literal found was not the negation
                    truthAssignment.append(lit)

                pureLiteralFound = True
                # Adding to a set the clause positions that contain the pure literals
                output.update(set(literalDict[lit][1:]))
                break

        # Converting the set to a list to sort it in reverse
        # This allows the removal of the clauses from back to front
        outputList = list(output)
        outputList.sort(reverse=True)
        for x in outputList:
            # Removing the clauses containing the pure literals
            clauses.pop(x)
        if pureLiteralFound:
            print("Applying pure literal rule")
            print(clauses)

    return clauses


def chooseLiteral(clauses):
    # Choosing the most frequent literal
    # In my case, the most frequent literal is the one which occurs most often dis regarding its sign
    # The return literal, i.e. the one worked on first, is either the positive or negative literal of the most frequent
    # one found based on which of the positive or negative literal was found frequent
    literalDict = {}
    for clause in clauses:
        for literal in clause:

            # Removing the negation if any
            if literal[0] == "!":
                literal = literal[1:]

            # Adding the literal (not including the ! if any) to a dictionary and adding 1 to the value.
            if literalDict.get(literal) is None:
                literalDict[literal] = 1
            else:
                literalDict[literal] += 1

    highestFrequency = 0

    for literal in literalDict:
        # Obtaining the highestFrequency literal
        if literalDict[literal] > highestFrequency:
            highestFrequencyLiteral = literal

    if literalDict.get("!" + highestFrequencyLiteral) is not None:
        # Checking if the positive or negative form of the highest frequency literal is more frequent
        if literalDict["!" + highestFrequencyLiteral] > literalDict[highestFrequencyLiteral]:
            highestFrequencyLiteral = "!" + highestFrequencyLiteral

    return highestFrequencyLiteral


def removeRepeatingLiteralInSameClause(clauses):
    # Checking for a removing any repeating literals in the same clause
    removed = True
    while (removed):
        removed = False
        for clause in clauses:
            # Adding each literal to a set
            for literal in clause:
                literalSet = set()
                literalSet.add(literal)

            # If the length of the length is not equal to the length of the clause than a repeating literal is found
            if len(literalSet) != len(clause):
                # This literal is searched for and removed
                for literal in clause:
                    if clause.count(literal) != 1:
                        clause.remove(literal)
                        removed = True
                        print("Removing repeating literal")
                        print(clauses)
                        break
                if removed:
                    break
    return clauses


def removeTautologyClause(clauses):
    # Checking for and removing tautology clauses
    removed = True
    while (removed):
        removed = False
        for c, clause in enumerate(clauses):
            # Checking that the clause only has two literals
            if len(clause) == 2:
                # Checking that one is the negation of the other
                if (clause[0] == ("!" + clause[1])) or (("!" + clause[0]) == clause[1]):
                    # Removing the clause
                    clauses.pop(c)
                    removed = True
                    print("Removing tautology clause")
                    print(clauses)
                    break

    return clauses


def DPLL(clauses):
    print("New iteration of DPLL")
    print(clauses)

    # Remove trivially satisfiable clauses
    removeTautologyClause(clauses)
    # Removing repeating literal
    removeRepeatingLiteralInSameClause(clauses)

    literalDict = {}
    # Checking for trivially unsatisfiable clauses
    for c, clause in enumerate(clauses):
        if len(clause) == 1:
            if addLiteralToDict(clause[0], c, 0, literalDict) != -1:
                # This means that we have found the opposite literal in two different clauses who's length is 1
                print("Trivially UNSAT")
                return False

    # Checking for empty clause
    for clause in clauses:
        if len(clause) == 0:
            print("Empty Clause found")
            return False

    # Checking for empty formula
    if len(clauses) == 0:
        return True

    # 1 Literal Rule
    change = True
    while change:
        change = False
        for clause in clauses:
            if len(clause) == 1:
                # Only clauses of length 1, having 1 literal are used
                change = True
                truthAssignment.append(clause[0])
                clauses = exhaustivelyApply1LiteralRule(clause[0], clauses)
                print("Applying 1 literal rule")
                print(clauses)

    # Checking for empty clause for applying 1 literal rule
    for clause in clauses:
        if len(clause) == 0:
            print("Empty Clause found")
            return False

    # Checking for empty set
    if len(clauses) == 0:
        return True

    # Exhaustively applying the pure literal rule
    clauses = exhaustivelyApplyPureLiteralRule(clauses)

    # Checking for empty clause for applying 1 literal rule
    for clause in clauses:
        if len(clause) == 0:
            print("Empty Clause found")
            return False

    # Checking for empty set
    if len(clauses) == 0:
        return True

    l = chooseLiteral(clauses)

    # Getting the opposite of the chosen literal
    if (l[0] == "!"):
        notl = l[1:]
    else:
        notl = "!" + l

    # Copying the list of clauses and adding the chosen literal and its negation to these copies and recursively calling DPLL using these new formulas.
    tempClauses1 = copy.deepcopy(clauses)
    tempClauses2 = copy.deepcopy(clauses)
    tempClauses1.append([l])
    tempClauses2.append([notl])
    newClausesWithPosLiteral = (tempClauses1).copy()
    newClausesWithNegLiteral = (tempClauses2).copy()

    return DPLL(newClausesWithPosLiteral) or DPLL(newClausesWithNegLiteral)


cnfInput = input("Enter CNF: ")
clauses = parseCNFInput(cnfInput)

if not DPLL(clauses):
    print("\nResult = UNSAT")
else:
    print("\nResult = SAT")
    print("\nTRUTH ASSIGNMENT")
    for ta in truthAssignment:
        print(ta, end="")
        print(" = True")

# Examples

# Notes
# SAT
# (x,!y,z)(u,v,y)(u,!x,!z)
# NOT SAT
# (x,y,z)(x,!y)(y,!z)(z,!x)(!x,!y,!z)


# http://sat.inesc-id.pt/~ines/sac10.pdf
# (a,!b,d)(a,!b,e)(!b,!d,!e)(a,b,c,d)(a,b,c,!d)(a,b,!c,e)(a,b,!c,!e)

# https://my.ece.utah.edu/~kalla/ECE6745/bool-sat.pdf
# (a,!b,!c)(!a,!b,!c)(b,c)(c,d)(c,!d)

# http://profs.sci.univr.it/~farinelli/courses/ar/slides/DPLL.pdf
# (p,q,!r)(p,!q)(!p)(r)(u)

# (p,!q)(!p,r)(q)

# (p,q)(p,!q)(!p,q)(!p,!r)

# ( p ,q )( !q)(!p, q, !r)

# http://www.dis.uniroma1.it/~liberato/ar/dpll/dpll.html
# (!x,!y)(!x,y)(x,!y)(y,!z)(x,z)

# ( !x, !y )(x,!y)(!x ,!z )

# (w,x,!y)(w,!x)(!w)(y)(z)