class Node():
    def __init__(self,character,weight):
        # Node object contains the character and weight, the left and right child
        self.character = character
        self.weight = weight
        self.leftChild = None
        self.rightChild = None

class Tree():
    def __init__(self,root):
        self.root = root

    def insertLeftChild(self,node):
        # Inserting the node in the left child and updating the tree's weight
        self.root.leftChild = node
        self.root.weight += node.root.weight

    def insertRightChild(self,node):
        # Inserting the node in the right child and updating the tree's weight
        self.root.rightChild = node
        self.root.weight += node.root.weight


    def produceCodingTable(self,root):
        # Empty string and dictionary
        currentCode = ""
        codingTable = {}

        # pre order traversal of the tree
        self.preOrderTraversal(root,currentCode,codingTable)
        return codingTable


    def preOrderTraversal(self,root,currentCode, codingTable):
        if root.leftChild is None and root.rightChild is None:
            # Leaf node found
            # Insert into the dictionary the character and the current code
            codingTable[root.character] = currentCode
            return currentCode

        if root.leftChild is not None:
            # When traversing a left child, add 0 to the code
            currentCode += "0"
            # Visiting the left child
            currentCode = self.preOrderTraversal(root.leftChild.root,currentCode,codingTable)
            # Removing the last 0
            currentCode = currentCode[:len(currentCode)-1]

        if root.rightChild is not None:
            # When traversing a right child, add 1 to the code
            currentCode += "1"
            # Visiting the right child
            currentCode = self.preOrderTraversal(root.rightChild.root,currentCode,codingTable)
            # Removing the last 1
            currentCode = currentCode[:len(currentCode) - 1]

        return currentCode

