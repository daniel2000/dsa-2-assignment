# Code adapted from
# https://dev.to/petercour/swap-keys-and-values-in-a-python-dictionary-1njn

import re
from Tree import *

def huffmanEncoding(characterDict):
    counter = 0
    forest = []

    for character in charcterDict:
        # Initializing a tree for each character
        forest.append(Tree(Node(character,charcterDict[character])))


    while len(forest) != 1:
        minWeight = float('inf')
        secondMinWeight = float('inf')
        minTree = float('inf')
        secondMinTree = float('inf')


        for t,tree in enumerate(forest):
            # Going through each tree in the forest
            if tree.root.weight < minWeight:
                # If the current tree has less weight than the current minimum
                if minWeight < secondMinWeight:
                    # The current minimum is checked whether it is smaller than the second minimum and if it is,
                    # the second minimum becomes the current first minimum
                    secondMinWeight = minWeight
                    secondMinTree = minTree

                # The weight and position of the newly found minimum tree are stored as the new minimum
                minTree = t
                minWeight = tree.root.weight
            elif tree.root.weight < secondMinWeight:
                # If the current tree has a weight larger than the current minimum but smaller than the current second minimum
                # The weight and position of the newly found minimum tree are stored as the new second minimum
                secondMinWeight = tree.root.weight
                secondMinTree = t

        # Initializing a new tree
        subTree = Tree(Node("T" + str(counter), 0))

        # Combine the two minimum trees
        subTree.insertLeftChild(forest[secondMinTree])
        subTree.insertRightChild(forest[minTree])

        # If the second minimum tree comes after the minimum tree in the list of trees (forest)
        # First remove the second minimum tree and then remove the minimum tree
        if secondMinTree>minTree:
            forest.pop(secondMinTree)
            forest.pop(minTree)
        elif secondMinTree<minTree:
            # If the minimum tree comes after the second minimum tree in the list of trees (forest)
            # First remove the minimum tree and then the second minimum tree
            forest.pop(minTree)
            forest.pop(secondMinTree)

        # Adding the newly created tree to the forest
        forest.append(subTree)

        counter += 1
    return forest[0]


def addCharacterToDict(charcater,dict):
    # Adding a character to the dictionary contiaing characters and their frequency
    if dict.get(charcater) is not None:
        # If the character already exists in the dictionary add 1 to the frequency
        dict[charcater] += 1
    else:
        # If the character doesn't exist in the dictionary set the frequency to 1
        dict[charcater] = 1


def checkAndAddInputToDict(fileStream):
    characterDict = {}
    # Going through each line in the file
    for line in fileStream:
        # Regular expression to only match A through Z, a through z, 0 through 9
        if re.match("^([A-Za-z0-9])+$", line):
            for character in line:
                addCharacterToDict(character,characterDict)
        else:
            # If the regular expression doesn't match an error is produced
            raise Exception("File contains characters other than A-Z a-z 0-9")

    return characterDict


# Opening the file
file = open("dataCompression.txt","r")
# Verifying the input and adding it to a dictionary
charcterDict = checkAndAddInputToDict(file)
# Producing the coding tree
resultTree = huffmanEncoding(charcterDict)

# Producing the coding table
codingTable = resultTree.produceCodingTable(resultTree.root)

#Sorting the character found by their ASCII value
characters = list(codingTable.keys())
sortedASIIValues = []
for char in characters:
    sortedASIIValues.append(ord(char))

sortedASIIValues.sort()

print("Character\tBinary Code")
# Displaying the coding table
for character in sortedASIIValues:
    print("'",chr(character),"'",end ="")
    print("\t:",end ="")
    print("\t" + codingTable[chr(character)])

